#This script will delete the images those matches the substring
import boto3
import collections
import datetime
import time

#AMI Name contains substring. Image name example: "Lambda - Test-app - From 09-06-2017"
include_match_image_name_substring1 = '09-06-2017' #Keep changing to match the substring and run the script
include_match_image_name_substring2 = 'Lambda'
exclude_match_image_name_substring3 = 'dr-mongo' #"Lambda - Test-dr-mongo - From 09-06-2017"

#specify the region in which EC2 Instances located and to cleanup AMI's. Ex: Singapore region (ap-southeast-1)
ec = boto3.client('ec2', 'ap-southeast-1')
ec2 = boto3.resource('ec2', 'ap-southeast-1')
images = ec2.images.filter(Owners=["XXXXX"])

def lambda_handler(event, context):

    to_tag = collections.defaultdict(list)

    date = datetime.datetime.now()
    date_fmt = date.strftime('%d-%m-%Y')
    print "Present date and time:" + date.strftime('%d-%m-%Y:%H.%m.%s')
    print "============="
    
    imagesList = []
    imagecount = 0
    
    # Loop through each image 
    for image in images:
        try:
            #Sort out AMI's finds an substring in image name
           # a = image.name.find(exclude_match_image_name_substring3)
            #print (a)
            #if (image.name.find(include_match_image_name_substring1)>=0):
            if ((image.name.find(include_match_image_name_substring1)>=0 and image.name.find(include_match_image_name_substring2)>=0) and (image.name.find(exclude_match_image_name_substring3)<0)):
            
                imagesList.append(image.id)
                print ("Found substring \"" + include_match_image_name_substring1 + " and " + include_match_image_name_substring2 + " \" not found \" " + exclude_match_image_name_substring3 + "\" in the image name \"" + image.name + "\"" + " and its image id is \"" + image.id + "\"")
                
                imagecount = imagecount + 1
  
        except Exception as e:
            print  "%s" %e.message
        
        
    print "============="
    print "About to process the " + str(imagecount) + " AMIs:"
    print imagesList

    snapshotList = []
    # Loop through each image 
    for image in imagesList:
        #print image
        desc_image_snapshots = ec.describe_images(ImageIds=[image],Owners=['XXXXX',])['Images'][0]['BlockDeviceMappings']
       # print (desc_image_snapshots)
        try:
            for desc_image_snapshot in desc_image_snapshots:
                snapshot = ec.describe_snapshots(SnapshotIds=[desc_image_snapshot['Ebs']['SnapshotId'],], OwnerIds=['XXXXX'])['Snapshots'][0]
                #if snapshot['Description'].find(image) > 0:
                snapshotList.append(snapshot['SnapshotId'])
                #else:
                #   continue
                #     print "Snapshot is not associated with an AMI"
                
        except Exception as e:
            print "Ignore Index Error:%s" % e.message
            
        print "Deregistering image %s" % image
        try:
            amiResponse = ec.deregister_image(
                    DryRun=False,
                ImageId=image,
            )
            #print "commented ami de-register"
        except Exception as e:
            print "%s" % e.message

    print "============="
        
    print "About to process the following Snapshots associated with above Images:"
    print (snapshotList)
        
    print "The timer is started for 5 seconds to wait for images to deregister before deleting the snapshots associated to it"    
    time.sleep(5)# This should be set to higher value if the image in the imagesList takes more time to deregister
        
    for snapshot in snapshotList:
        try:
            snap = ec.delete_snapshot(SnapshotId=snapshot)
            print "Deleted snapshot " + snapshot
            
        except Exception as e:
            print "%s" % e.message
    print "-------------"
