#With Pagination:
#This script will parse the Cloud Trail logs for Route53 Record Set changes and notify using SNS target 

import boto3
import datetime
import time
from datetime import datetime as dt
import json
import sys

today = datetime.datetime.now()
year_today = today.strftime('%Y')
month_today = today.strftime('%m')
day_today = today.strftime('%d')
hour_now = today.strftime('%H')
minute_now = today.strftime('%M')
today_strip = today.strftime('%d-%m-%Y') # you can append time format as well, :%H:%M 

yesterday = today - datetime.timedelta(days=1) 
year_yesterday = yesterday.strftime('%Y')
month_yesterday = yesterday.strftime('%m')
day_yesterday = yesterday.strftime('%d')
yesterday_strip = yesterday.strftime('%d-%m-%Y') # you can append time as well, 18:00 in UTC means 12AM in IST

#For Amazon Route 53, CloudTrail captures information about API requests in US East (N. Virginia) as the region.
ct_client = boto3.client('cloudtrail','us-east-1')

#SNS Client and Topic end point to notify the events 
sns_client = boto3.client('sns', 'ap-south-1')
SNS_TOPIC_ARN = "arn:aws:sns:ap-south-1:XXXXX:SNS_Notif"

def lambda_handler(event, context):
    #list gives upto last 7 days of API activity
    marker = None
    
    paginator = ct_client.get_paginator('lookup_events')
    
    response_iterator = paginator.paginate(
        LookupAttributes=[
            {
                'AttributeKey':'EventName',
                'AttributeValue': 'ChangeResourceRecordSets'
            },
        ],
        #Specify the time based on the trigger time
        StartTime=dt(int(year_yesterday), int(month_yesterday), int(day_yesterday), 02, 30),#02:30 in UTC means 7:00AM in IST
        EndTime=dt(int(year_today), int(month_today), int(day_today), int(hour_now), int(minute_now)),
            PaginationConfig={
                'PageSize': 10,
                'StartingToken': marker
        }
    )
   
    f = open('/tmp/record_sets_out.txt', 'w')
    i=1
    for page in response_iterator:
    #print("Next Page : {} ".format(page['IsTruncated']))
        route53_api_events = page['Events']
             
        for route53_api_event in route53_api_events:
            cloudtrailevent_inventory  = json.loads(route53_api_event['CloudTrailEvent'])
        
            f.write("\nChanged Route53 ResourceRecordSets Number------>: %d\r" %(i))
            f.write("\nEvent Name------------------------------------->: %s\r" %cloudtrailevent_inventory['eventName'])
            f.write("\nResourceRecordSets Changed for the Hosted Zone->: %s\r" %cloudtrailevent_inventory['requestParameters']['hostedZoneId'])
            f.write("\nEvent Occurred Time---------------------------->: %s\r" %cloudtrailevent_inventory['eventTime'])
            f.write("\nUser who made the RecordSet(s) changes--------->: %s\r" %cloudtrailevent_inventory['userIdentity']['userName'])
            f.write("\nSourceIPAddress from which User logged in------>: %s\r" %cloudtrailevent_inventory['sourceIPAddress'])
            f.write("\nAgent Used by the logged in user--------------->: %s\r" %cloudtrailevent_inventory['userAgent'])
            f.write("\nResourceRecordSets Changes are----------------->: %s\r" %cloudtrailevent_inventory['requestParameters']['changeBatch']['changes'])
            f.write("\nResourceRecordSets ChangeInfo status----------->: %s\r\n" %cloudtrailevent_inventory['responseElements']['changeInfo']['status'])
    
            i = i+1

    f.close()
    f=open("/tmp/record_sets_out.txt", "r")
    if f.mode == 'r':
        contents =f.read()
        print (contents)
    
    try:
       sns_response = sns_client.publish(
          TopicArn=SNS_TOPIC_ARN,
         Message=contents,
            Subject='AWS Notification - List of Route53 Recordset Changes between '+yesterday_strip+' and '+today_strip
        )
        print (sns_response)

        
    except Exception as e:
        sns_response = sns_client.publish(
          TopicArn=SNS_TOPIC_ARN,
         Message="None of the Route53 Record Sets are being changed!",
            Subject='AWS Notification - List of Route53 Recordset Changes between '+yesterday_strip+' and '+today_strip
        )
        print (sns_response)
      
    try:
        marker = page['Marker']
        print(marker)
    except KeyError:
       # sys.exit()
        print ("exited")
