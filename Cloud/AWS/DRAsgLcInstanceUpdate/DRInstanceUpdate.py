import boto3
import collections
import datetime
import time
import base64
 
#Global objects 
#ec2 = boto3.client('ec2', 'ap-southeast-1')
ec = boto3.resource('ec2', 'ap-southeast-1')
images = ec.images.filter(Owners=["XXXXXX"])# Specify your AWS account owner id in place of "XXXXX" at all the places in this script
autoscale = boto3.client('autoscaling', 'ap-southeast-1')


##To sort out the ami's date 
today = datetime.datetime.now()
#To sort out the ami's that matches the date to be updated in the new launch configuration
days_old = today - datetime.timedelta(days=1) # Zero means todays date and 1 means yesterday  
days_old_date = days_old.strftime('%d-%m-%Y')

#Keep adding more number of AMI prefix names created from lambda here
apache_app = 'Lambda - '+'Apache-app' # 'Apache-app' can be the prefix name for ASG Ex: Apache-app-DR-ASG
tomcat_app = 'Lambda - '+'Tomcat-app'

#The variable should be comma separated
image_names_array = [apache_app,tomcat_app]

#To update in Auto-scaling, You can change to keep min instances during DR. 
MIN_SIZE=2
DESIRED_CAPACITY=2
MAX_SIZE=2

def lambda_handler(event, context):
    imagesList = []
    
    for image_name in image_names_array:    
      for image in images:
        try:    
            if ((image.name.startswith(image_name)) and (image.name.endswith(days_old_date))):    
                imagesList.append(image.name)
                #print ("array image_name %s" %image_name)
				#new_ami_id = image.id
                print  ("\nThe new AMI to be used with new LC is: \"%s\"" %image.name)
                
                #Existing Autoscaling group details, ASG be must be manually created.
                #lambda_prefix_asg_name = trunc_at(image.name, "-",2) # Ex: Lambda - Apache
                lambda_prefix_asg_name = trunc_at(image.name, "-",3) # Ex: Lambda - Apache-app
                ASG_NAME = lambda_prefix_asg_name.replace('Lambda - ', '')+'-DR-ASG'
                
                try: 
                    autoscale_group = autoscale.describe_auto_scaling_groups(
                            AutoScalingGroupNames= [
                                ASG_NAME 
                                ]
                    )
                    
                    
                    autoscale_lc_name = [aslcn['LaunchConfigurationName'] for aslcn in autoscale_group['AutoScalingGroups'] if 'LaunchConfigurationName' in aslcn][0]
                    print ("The Autoscaling group is: %s" % ASG_NAME) # Ex: Apache-app-DR-ASG 
                    print ("The current LC being used in autoscaling is: %s" %autoscale_lc_name)
                    
                    lc_config = autoscale.describe_launch_configurations(
                        LaunchConfigurationNames=[
                                 autoscale_lc_name
                            
                        ],
                    )
                    #Save the current lc details to variables
                    print ("\nThe configurations to create new LC are below......")
                    key_pair    = [lck['KeyName'] for lck in lc_config['LaunchConfigurations'] if 'KeyName' in lck][0]
                    print ("Current LC key-pair: %s" % key_pair) 
                    instance_type = [lcit['InstanceType'] for lcit in lc_config['LaunchConfigurations'] if 'InstanceType' in lcit][0]
                    print ("Current LC instance type: %s" % instance_type)
                    space_old_launch_config_name = [lcn['LaunchConfigurationName'] for lcn in lc_config['LaunchConfigurations'] if 'LaunchConfigurationName' in lcn][0]
                    old_launch_config_name = space_old_launch_config_name.replace(" ", "") # Without white spaces
                    print ("Current launch config name: %s" % old_launch_config_name)
					#old_ami_id = [lcn['ImageId'] for lcn in lc_config['LaunchConfigurations'] if 'ImageId' in lcn][0]
                    security_group_id = [lcsgi['SecurityGroups'][0] for lcsgi in lc_config['LaunchConfigurations'] if 'SecurityGroups' in lcsgi][0]
                    print ("Current LC instance type: %s" %security_group_id)
                    new_ami_id = image.id
                    print ("New AMI Id to be updated: %s" %new_ami_id) 
                    encoded_user_data = [lcud['UserData'] for lcud in lc_config['LaunchConfigurations'] if 'UserData' in lcud][0]
                    #to_string = "\""+encoded_user_data+"\""
                    decoded_user_data =  base64.b64decode(encoded_user_data)
                    print ("-------------------------------------------------------------------------------------------------")
                    print ("Current LC encoded User data:\n %s" % encoded_user_data)
                    print ("\nCurrent LC decoded data is:\n %s" % decoded_user_data)
                    print ("-------------------------------------------------------------------------------------------------")
                    try:
                        iam_instance_profile = [lcud['IamInstanceProfile'] for lcud in lc_config['LaunchConfigurations'] if 'IamInstanceProfile' in lcud][0]
                        print ("Current IamInstanceProfile : %s" %iam_instance_profile)
                    except Exception as e:
                        iam_instance_profile = None  
                                        
                    #To extract todays date to append with the new launch configuration name    
                    #print (days_old_date)
                    todays = today - datetime.timedelta(days=0)
                    today_date = todays.strftime('%d-%m-%Y')
                    #print (today_date)
                    #print (str(old_launch_config_name))
                    #space_new_launch_config_name = old_launch_config_name.replace(str(days_old_date), str(today_date))
                    new_launch_config_name = image_name.replace(" ", "")+'-'+today_date+'-LC' # Without white spaces
                    print ("\nThe new launch configuration name is: %s" %new_launch_config_name)
                   
                    try:
                        if iam_instance_profile is None:
                            create_launch_config = autoscale.create_launch_configuration(
                                LaunchConfigurationName=new_launch_config_name,
                                ImageId=new_ami_id,
                                KeyName=key_pair,
                                SecurityGroups=[
                                security_group_id,
                                ],
                                InstanceType=instance_type,
                                AssociatePublicIpAddress=True,
                            # UserData = user_data,
                                UserData =decoded_user_data
                            )  
                        else:
                            create_launch_config = autoscale.create_launch_configuration(
                                LaunchConfigurationName=new_launch_config_name,
                                ImageId=new_ami_id,
                                KeyName=key_pair,
                                SecurityGroups=[
                                security_group_id,
                                ],
                                InstanceType=instance_type,
                                AssociatePublicIpAddress=True,
                            # UserData = user_data,
                                UserData =decoded_user_data,
                                IamInstanceProfile=iam_instance_profile
                            )  
                        print ("The new LC being successfully created is %s" % new_launch_config_name)

                    except Exception as e:
                        print "Failed to create new LC. %s" % e.message
                    
                    try:
                        update_autoscale_group = autoscale.update_auto_scaling_group(
                            AutoScalingGroupName=ASG_NAME,
                            MinSize=int(MIN_SIZE), 
                            DesiredCapacity=int(DESIRED_CAPACITY),# Launch instance with the new config
                            MaxSize=int(MAX_SIZE),
                            LaunchConfigurationName=new_launch_config_name
                        )
                        print ("The ASG %s updated with MIN_SIZE:%d, DESIRED_CAPACITY:%d, MAX_SIZE:%d " %(ASG_NAME,MIN_SIZE,
                     DESIRED_CAPACITY,MAX_SIZE))

                        
                    except Exception as e:
                        print "Failed to update ASG with new LC. %s" % e.message
                    
                    
                    try:
                        delete_launch_config = autoscale.delete_launch_configuration(
                           LaunchConfigurationName=old_launch_config_name
                        )
                        print "uncomment above and delete this line in prod"
                    except Exception as e:
                        print "%s" % e.message
                        
                    print ("%d AMI(s) are successfully being updated with the new launch configurations which are: %s" %len(imagesList) %(imagesList))
                    
					#To delete the ami tag to retain the image 
					#delete_ami_tag = ec2.delete_tags(
					#	Resources=[new_ami_id],
					#	Tags=[
					#		{'Key': 'DeleteOnCopy'},
					#	]
					#)
                    #To create tag to delete the AMI once its removed from LC
					#delete_date = datetime.date.today() + datetime.timedelta(days=1)
					#delete_fmt = delete_date.strftime('%d-%m-%Y')
					
					#create_ami_tag = ec2.create_tags(
					#	Resources=[old_ami_id],
					#	Tags=[
					#		{key': 'DeleteOnCopy', 'Value': delete_fmt},
                    #	]
					#)
 
                except Exception as e:
                    print "ignore index error. %s" %e.message 
                    
            
            else:
                continue
    
        except Exception as e:
            print "image not found, ignore. %s" %e.message

def trunc_at(s, d, n=3):
    "Returns s truncated at the n'th (3rd by default) occurrence of the delimiter, d."
    return d.join(s.split(d)[:n])
    
